import gkeepapi
import os
from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/item', methods=['POST'])
def add_item():
    item = request.json['item']

    # Create a gkeepapi client and authenticate
    keep = gkeepapi.Keep()
    keep.login(os.getenv('KEEP_USER'), os.getenv('KEEP_PASSWORD'))
    # Get the Google Keep list
    gnote = keep.get(os.getenv('KEEP_NOTE_ID'))

    # Add item to list
    gnote.add(item, False)
    keep.sync()

    return jsonify({'success': True})

if __name__ == '__main__':
    # Run the app on port 6000
    app.run(host="0.0.0.0", port=6000)

