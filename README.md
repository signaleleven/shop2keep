Super simple http interface to add items to google keep
---

This code is bad. This idea is bad. But it works for me.
It exposes an endpoint to write items to a specific Google Keep list. That's it. No authentication (on the endpoint, it manages authenticating to google). No nothing.

I used to do this from an appdaemon application but it stopped working because of https://github.com/kiwiz/gkeepapi/issues/144

I use this from an appdaemon app to move items from the hass shopping list to google keep.


It requires a `.env` file shaped like this

```
KEEP_PASSWORD=yourpassword 
KEEP_NOTE_ID=keep_note_id
KEEP_USER=youruser@gmail.com
```
I recommend sharing the note with a google user created only for this. Even an application password would be too powerful.


Example appdaemon app that reacts to an item being added to the shopping list, passes it to this project and then deletes the item from home assistant:

```python

import appdaemon.plugins.hass.hassapi as hass
import requests
import json


class GoogleKeepUpdater(hass.Hass):

    def initialize(self):
        self.listen_event(self.update_list, 'shopping_list_updated')
        self.log("Hello from Gkeeper")


    def update_list(self, event_id, payload_event, *args):
            if payload_event['action'] == "add":
                item=payload_event['item']['name']
                self.log(item)
                result = requests.post(
                        url='http://[WHEREVER YOU DEPLOY THIS]:6000/item',
                        data=json.dumps({'item': item}),
                        headers = {'Content-type': 'application/json'}
                        )
                if result.status_code == 200:
                    self.call_service('shopping_list/complete_item', name=item)
                    self.call_service('shopping_list/clear_completed_items')

```


All of this would be easier if google keep had a better user API or if I could be arsed to try the official one (developer api). As I said. This "works for me".